class AddStatusToHiring < ActiveRecord::Migration
  def change
    add_column :hirings, :open, :boolean, :default => true
  end
end
