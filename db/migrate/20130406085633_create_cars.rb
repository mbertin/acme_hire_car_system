class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :label
      t.integer :daily_price

      t.timestamps
    end
  end
end
