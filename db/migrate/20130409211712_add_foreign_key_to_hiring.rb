class AddForeignKeyToHiring < ActiveRecord::Migration
  def change
    add_column :hirings, :car_id, :integer
    add_column :hirings, :user_id, :integer
  end
end
