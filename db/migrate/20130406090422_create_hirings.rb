class CreateHirings < ActiveRecord::Migration
  def change
    create_table :hirings do |t|
      t.date :hire_date
      t.integer :nb_days

      t.timestamps
    end
  end
end
