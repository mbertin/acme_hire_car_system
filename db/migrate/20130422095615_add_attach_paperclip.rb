class AddAttachPaperclip < ActiveRecord::Migration
  	def up
		add_column :cars, :pic_file_name,    :string
		add_column :cars, :pic_content_type, :string
		add_column :cars, :pic_file_size,    :integer
		add_column :cars, :pic_updated_at,   :datetime
  	end

	def down
		remove_column :cars, :pic_file_name,    :string
		remove_column :cars, :pic_content_type, :string
		remove_column :cars, :pic_file_size,    :integer
		remove_column :cars, :pic_updated_at,   :datetime
	end
end
