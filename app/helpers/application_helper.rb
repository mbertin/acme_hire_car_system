module ApplicationHelper

	def title
		base_title = "ACME Hire Car System"
		if @title.nil?
		  base_title
		else
		  "#{base_title} | #{@title}"
		end
	end
	
end
