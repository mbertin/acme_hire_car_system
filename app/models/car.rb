# == Schema Information
#
# Table name: cars
#
#  id          :integer          not null, primary key
#  label       :string(255)
#  daily_price :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Car < ActiveRecord::Base
 	has_many :hirings
   	attr_accessible :daily_price, :label, :pic
	has_attached_file :pic, :styles => { :medium => "300x300>", :thumb => "100x100>" }
end