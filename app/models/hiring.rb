# == Schema Information
#
# Table name: hirings
#
#  id          :integer          not null, primary key
#  hire_date   :date
#  nb_days     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  car_id      :integer
#  customer_id :integer
#

class Hiring < ActiveRecord::Base
	belongs_to :car
	belongs_to :user
  	attr_accessible :hire_date, :nb_days, :car_id, :user_id

  	validates :car_id, :presence => true, :if => :is_car_available

  	def price
		car =  Car.find(self.car_id)
		amount = car.daily_price * self.nb_days
		
		return amount
	end

	def extra_fees
		extras_fees = 0
		if DateTime.now > self.return_date
			logger.debug "#{DateTime.now - self.return_date}"
			extra_days = (DateTime.now - self.return_date).to_i
			extras_fees= 2 * (extra_days * car.daily_price) 
		end
		return extras_fees
	end

	def return_date
		return self.hire_date + self.nb_days.days
	end

	def customer_name
		customer = User.find(self.user_id)
		return customer.name
	end

	def car_label
		car = Car.find(self.car_id)
		return car.label
	end

	def is_car_available
		car =  Car.find(self.car_id)
		hiringsWithCar = car.hirings

		range = self.hire_date..self.return_date
		ret=true
		if !hiringsWithCar.nil?
			
			hiringsWithCar.each{|hiring|
				if(range.cover?(hiring.hire_date) || range.cover?(hiring.return_date))
					ret=false
					break
				end
			}
		end
		return ret
	end
end
