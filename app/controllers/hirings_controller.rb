class HiringsController < ApplicationController
  # GET /hirings
  # GET /hirings.json
  def index
    @hirings = Hiring.all

    logger.debug "#{@hirings}"
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @hirings }
    end
  end

  # GET /hirings/1
  # GET /hirings/1.json
  def show
    @hiring = Hiring.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @hiring }
    end
  end

  # GET /hirings/new
  # GET /hirings/new.json
  def new
    @hiring = Hiring.new params[:id]
    @cars = Car.find(:all)
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @hiring }
    end
  end

  # GET /hirings/1/edit
  def edit
    @hiring = Hiring.find(params[:id])
    @cars = Car.find(:all)
  end

  # POST /hirings
  # POST /hirings.json
  def create
    @hiring  = current_user.hirings.build(params[:hiring])
    @cars = Car.find(:all)

    if @hiring.is_car_available
      respond_to do |format|
        if @hiring.save
          format.html { redirect_to @hiring, notice: 'You have successfully hire a car.' }
          format.json { render json: @hiring, status: :created, location: @hiring }
        else
          format.html { render action: "new" }
          format.json { render json: @hiring.errors, status: :unprocessable_entity }
        end
      end
    else       
      flash.now[:error] = " Car isn't available for the specified date! Please chose another!"
      render 'new'
    end
  end

  # PUT /hirings/1
  # PUT /hirings/1.json
  def update
    @hiring = Hiring.find(params[:id])

    respond_to do |format|
      if @hiring.update_attributes(params[:hiring])
        format.html { redirect_to @hiring, notice: 'Hiring was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @hiring.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hirings/1
  # DELETE /hirings/1.json
  def destroy
    @hiring = Hiring.find(params[:id])
    @hiring.destroy

    respond_to do |format|
      format.html { redirect_to hirings_url }
      format.json { head :no_content }
    end
  end

  def close
    @hiring = Hiring.find(params[:hiring_id])
    @hiring.open = false
    respond_to do |format| 
      if @hiring.save
        format.html { redirect_to hirings_url, notice: 'Hire Close.' }
        format.json { render json: @hiring, status: :updated, location: @hiring }
      else
        flash.now[:error] = "Ooops! An error occured, please retried!"
        format.html { redirect_to hirings_url }
      end
    end
  end
end
