class SessionsController < ApplicationController

	# POST /sessions
	def create
		logger.debug "Create Session => Signin"
		logger.debug "#{params[:session][:email]}"
		logger.debug "#{params[:session][:password]}"

 		user = User.authenticate(params[:session][:email],
				                 params[:session][:password])

	    if user.nil?
			render :text => "false"
	    else
     		sign_in user
			render :text => "true"
	    end
	end

	def destroy
		logger.debug "Deconnect"
		sign_out
		#render :js "$('#messageSucces').html= You are now disconnected ! "
    	redirect_to home_path
	end
end
