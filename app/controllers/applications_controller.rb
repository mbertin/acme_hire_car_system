class ApplicationsController < ApplicationController
  
  def home
      @title = "Home Page"
      @cars = Car.all
  end

  def connect
  end

  def browseCars
      @title = "Our Cars"
      @cars = Car.all
  end

  def disconnect
  end

  def bookCar
     @cars = Car.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @cars }
    end
  end
end
