/***
	ACME HIRE CAR SYSTEM
***/

function init()
{
	registerLoginHandler();
	$('.carousel').carousel('next')
	//registerSlideShow();
}

function registerSlideShow(){
	 $('#slideshowHolder').jqFancyTransitions({ width: 400, height: 300 });
}

function registerLoginHandler()
{
	$("a#login").on("click", function(event)
	{
		displayLoginModal();
	});
}

function displayLoginModal()
{
	$('#loginModal').modal();
	$("#loginButton").on("click",function() {
  		$('#loginForm').submit();
	});

	$("#loginForm").submit(function(event) {

	    event.preventDefault();
		$('#loginButton').toggle();
	    var $form = $( this ),
	        email = $form.find('input[name="session[email]"]').val(),
	        password = $form.find('input[name="session[password]"]').val();
			url = $form.attr( 'action' );
		
	    $.post( url, {session : { email: email, password:password }},
	      	function( data ) {
	      		if(data == "false")
	      		{
	      			$('#loginMessage').html('Authentication failed! Please Try Again!');
	      			$('#loginMessage').removeClass('hide');
   					$('#loginButton').toggle();
	      		}
	      		else
	      		{
	      			$('#loginModal').modal('hide');
	      			$('#loginMessage').addClass('hide');
	  				location.reload(true);
	      		}
	      	}
	    );
  	});
}

$(document).ready(function() {
	init();
});