require 'test_helper'

class ApplicationsControllerTest < ActionController::TestCase
  test "should get connect" do
    get :connect
    assert_response :success
  end

  test "should get browseCar" do
    get :browseCar
    assert_response :success
  end

  test "should get disconnect" do
    get :disconnect
    assert_response :success
  end

  test "should get bookCar" do
    get :bookCar
    assert_response :success
  end

end
