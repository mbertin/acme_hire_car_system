require 'test_helper'

class HiringsControllerTest < ActionController::TestCase
  setup do
    @hiring = hirings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:hirings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create hiring" do
    assert_difference('Hiring.count') do
      post :create, hiring: { extras_fees: @hiring.extras_fees, hire_date: @hiring.hire_date, nb_days: @hiring.nb_days }
    end

    assert_redirected_to hiring_path(assigns(:hiring))
  end

  test "should show hiring" do
    get :show, id: @hiring
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @hiring
    assert_response :success
  end

  test "should update hiring" do
    put :update, id: @hiring, hiring: { extras_fees: @hiring.extras_fees, hire_date: @hiring.hire_date, nb_days: @hiring.nb_days }
    assert_redirected_to hiring_path(assigns(:hiring))
  end

  test "should destroy hiring" do
    assert_difference('Hiring.count', -1) do
      delete :destroy, id: @hiring
    end

    assert_redirected_to hirings_path
  end
end
