AcmeHireCarSystem::Application.routes.draw do

  resources :hirings
  resources :cars
  resources :users
  resources :sessions, :only => [:create, :destroy]


  match '/', :to => 'applications#home'
  match '/about',   :to => 'pages#about'
  match '/browseCar', :to => 'cars#index'
  match '/help',    :to => 'pages#help'
  match '/home', :to => 'applications#home'
  match '/newCar', :to => 'cars#new'
  match '/signin',  :to => 'sessions#create'
  match '/signout', :to => 'sessions#destroy'
  match '/signup',  :to => 'users#new'
  match '/bookCar', :to => 'hirings#new'
  match '/user_hirings', :to =>"users#listHirings"
  match '/hirings', :to => "hirings#index"
  match '/close_hiring', :to => "hirings#close"
  root :to => 'pages#home'

 
=begin
  get "applications/home"
  get "applications/connect"
  get "applications/browseCar"
  get "applications/disconnect"
  get "applications/bookCar"
=end
#Resources Used
#

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
  #root :to => 'hirings#index'
end
