require 'spec_helper'

describe User do

  	before(:each) do
    	@attr = { :name => "Example User", 
    			  :email => "user@example.com"
    			}
  	end

  	it "Should create a entity with the specified attributes" do
    	User.create!(@attr)
  	end

  	it "Should need a name" do
  		bad_guy = User.new(@attr.merge(:name => ""))
    	bad_guy.should_not be_valid
  	end

	it "Should reject username too long" do
	    long_name = "a" * 51
	    long_name_user = User.new(@attr.merge(:nom => long_name))
	    long_name_user.should_not be_valid
	end
end